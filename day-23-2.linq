<Query Kind="Program" />

static string input = @"cpy a b
dec b
cpy a d
cpy 0 a
cpy b c
inc a
dec c
jnz c -2
dec d
jnz d -5
dec b
cpy b c
cpy c d
dec d
inc c
jnz d -2
tgl c
cpy -16 c
jnz 1 c
cpy 77 c
jnz 87 d
inc a
inc d
jnz d -2
inc c
jnz c -5";

void Main()
{
	var registers = new Dictionary<string, int>
	{
		{ "a", 12 },
		{ "b", 0 },
		{ "c", 0 },
		{ "d", 0 },
	};
	var instructions = input.Split(new[] { Environment.NewLine }, StringSplitOptions.None);
	int instructionPointer = 0;
	while (instructionPointer < instructions.Length)
	{
		var instruction = instructions[instructionPointer];
		var tokens = instruction.Split(new[] { ' ' });
		int x = 0;
		int y = 0;
		switch (tokens[0])
		{
			case "cpy":
				if (!Int32.TryParse(tokens[1], out x))
				{
					x = registers[tokens[1]];
				}
				if (registers.Keys.Contains(tokens[2]))
				{
					registers[tokens[2]] = x;
				}
				instructionPointer++;
				break;
			case "inc":
				registers[tokens[1]] = registers[tokens[1]] + 1;
				instructionPointer++;
				break;
			case "dec":
				registers[tokens[1]] = registers[tokens[1]] - 1;
				instructionPointer++;
				break;
			case "jnz":
				if (!Int32.TryParse(tokens[1], out x))
				{
					x = registers[tokens[1]];
				}
				if (x == 0)
				{
					instructionPointer++;
				}
				else
				{
					if (!Int32.TryParse(tokens[2], out y))
					{
						y = registers[tokens[2]];
					}
					instructionPointer += y;
				}
				break;
			case "tgl":
				if (!Int32.TryParse(tokens[1], out x))
				{
					x = registers[tokens[1]];
				}
				var targetInstruction = instructionPointer + x;
				if (targetInstruction >= 0 && targetInstruction < instructions.Length)
				{
					instructions[targetInstruction] = ToggleInstruction(instructions[targetInstruction]);
				}
				instructionPointer++;
				break;
		}
	}
	registers["a"].Dump();
}

string ToggleInstruction(string original)
{
	var tokens = original.Split(new[] { ' ' });
	switch (tokens[0])
	{
		case "inc":
			return original.Replace("inc", "dec");
		case "dec":
			return original.Replace("dec", "inc");
		case "tgl":
			return original.Replace("tgl", "inc");
		case "jnz":
			return original.Replace("jnz", "cpy");
		case "cpy":
			return original.Replace("cpy", "jnz");
	}
	return null;
}
