<Query Kind="Program" />

// elevator, type 1 generator, type 1 microchip, type 2 elevator, type 2 microchip...
static byte[] input = new[] { (byte)0, (byte)0, (byte)0, (byte)0, (byte)1, (byte)0, (byte)1, (byte)2, (byte)2, (byte)2, (byte)2, (byte)0, (byte)0, (byte)0, (byte)0 };

class StateWithSteps
{
	public byte[] State { get; private set; }
	public int Steps { get; private set; }

	public StateWithSteps(byte[] state, int steps)
	{
		State = state;
		Steps = steps;
	}
}

public int Hash(byte[] state)
{
	var hash = 0;
	for (int i = 0; i < state.Length; i++)
	{
		hash = hash * 4 + state[i];
	}
	return hash;
}

public bool IsDone(byte[] state)
{
	for (int i = 0; i < state.Length; i++)
	{
		if (state[i] != 3)
		{
			return false;
		}
	}
	return true;
}

public byte[] Clone(byte[] state)
{
	var newState = new byte[state.Length];
	Array.Copy(state, newState, state.Length);
	return newState;
}

public IEnumerable<byte[]> GetCandidates(byte[] state)
{
	// get minimum floor with any items left
	var minFloor = state.Min();
	// single items up or down
	for (int i = 1; i < state.Length; i++)
	{
		if (state[0] == state[i])
		{
			// move up
			if (state[0] < 3)
			{
				var newState = Clone(state);
				newState[0]++;
				newState[i]++;
				yield return newState;
			}
			// move down (only if all floors below are not already empty)
			if (state[0] > minFloor)
			{
				var newState = Clone(state);
				newState[0]--;
				newState[i]--;
				yield return newState;
			}
		}
	}
	// two items only up
	if (state[0] < 3)
	{
		// matching generator and microchip pairs
		for (int i = 1; i < state.Length; i = i + 2)
		{
			if (state[i] == state[0] && state[i + 1] == state[0])
			{
				var newState = Clone(state);
				newState[0]++;
				newState[i]++;
				newState[i + 1]++;
				yield return newState;
			}
		}
		// generator pairs and microchip pairs
		for (int i = 0; i <= 1; i++)
		{
			var indices = new List<int>();
			for (int j = 1 + i; j < state.Length; j = j + 2)
			{
				if (state[0] == state[j])
				{
					indices.Add(j);
				}
			}
			if (indices.Count >= 2)
			{
				for (int first = 0; first < indices.Count - 1; first++)
				{
					for (int second = first + 1; second < indices.Count; second++)
					{
						var newState = Clone(state);
						newState[0]++;
						newState[indices[first]]++;
						newState[indices[second]]++;
						yield return newState;
					}
				}
			}
		}
	}
}

public bool IsValid(byte[] state)
{
	// check all microchips
	for (int i = 2; i < state.Length; i = i + 2)
	{
		if (state[i - 1] == state[i])
		{
			// corresponding generator in same floor, it's ok
			continue;
		}
		else
		{
			// check all generators, we already know matching generator is in different floor, therefore it's ok to check it, too
			for (int j = 1; j < state.Length; j = j + 2)
			{
				if (state[j] == state[i])
				{
					// other generator in same floor would fry microchip
					return false;
				}
			}
		}
	}
	return true;
}

void Main()
{
	var nextStates = new Queue<StateWithSteps>();
	var visited = new HashSet<int>();

	nextStates.Enqueue(new StateWithSteps(input, 0));
	visited.Add(Hash(input));

	while (nextStates.Count > 0)
	{
		var current = nextStates.Dequeue();
		foreach (var candidate in GetCandidates(current.State))
		{
			var hash = Hash(candidate);
			if (visited.Contains(hash))
			{
				continue;
			}
			else
			{
				visited.Add(hash);
			}
			if (IsDone(candidate))
			{
				(current.Steps + 1).Dump();
				return;
			}
			if (IsValid(candidate))
			{
				nextStates.Enqueue(new StateWithSteps(candidate, current.Steps + 1));
			}
		}
	}
}