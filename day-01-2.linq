<Query Kind="Program" />

static string input = "R4, R3, R5, L3, L5, R2, L2, R5, L2, R5, R5, R5, R1, R3, L2, L2, L1, R5, L3, R1, L2, R1, L3, L5, L1, R3, L4, R2, R4, L3, L1, R4, L4, R3, L5, L3, R188, R4, L1, R48, L5, R4, R71, R3, L2, R188, L3, R2, L3, R3, L5, L1, R1, L2, L4, L2, R5, L3, R3, R3, R4, L3, L4, R5, L4, L4, R3, R4, L4, R1, L3, L1, L1, R4, R1, L4, R1, L1, L3, R2, L2, R2, L1, R5, R3, R4, L5, R2, R5, L5, R1, R2, L1, L3, R3, R1, R3, L4, R4, L4, L1, R1, L2, L2, L4, R1, L3, R4, L2, R3, L1, L5, R4, R5, R2, R5, R1, R5, R1, R3, L3, L2, L2, L5, R2, L2, R5, R5, L2, R3, L5, R5, L2, R4, R2, L1, R3, L5, R3, R2, R5, L1, R3, L2, R2, R1";

enum Direction
{
	North = 0,
	East = 1,
	South = 2,
	West = 3
}

struct Location
{
	public int x;
	public int y;
	
	public Location(Location original)
	{
		this.x = original.x;
		this.y = original.y;
	}
	public override string ToString()
	{
		return $"{x},{y}";
	}
}

void Main()
{
	var commands = input.Split(new[] { ", " }, StringSplitOptions.None);
	var location = new Location();
	var direction = Direction.North;
	var previousLocations = new HashSet<Location>();
	previousLocations.Add(new Location());
	foreach (var command in commands)
	{
		direction = (Direction)(((int)direction + (command[0] == 'R' ? 1 : -1) + 4) % 4);
		var distance = Int32.Parse(command.Substring(1));
		while (distance > 0)
		{
			switch (direction)
			{
				case Direction.North:
					location.y++;
					break;
				case Direction.East:
					location.x++;
					break;
				case Direction.South:
					location.y--;
					break;
				case Direction.West:
					location.x--;
					break;
			}
			distance--;
			if (previousLocations.Contains(location))
			{
				var result = Math.Abs(location.x) + Math.Abs(location.y);
				result.Dump();
				return;
			}
			previousLocations.Add(new Location(location));
		}
	}
}
