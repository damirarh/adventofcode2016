<Query Kind="Program" />

static string input = "R4, R3, R5, L3, L5, R2, L2, R5, L2, R5, R5, R5, R1, R3, L2, L2, L1, R5, L3, R1, L2, R1, L3, L5, L1, R3, L4, R2, R4, L3, L1, R4, L4, R3, L5, L3, R188, R4, L1, R48, L5, R4, R71, R3, L2, R188, L3, R2, L3, R3, L5, L1, R1, L2, L4, L2, R5, L3, R3, R3, R4, L3, L4, R5, L4, L4, R3, R4, L4, R1, L3, L1, L1, R4, R1, L4, R1, L1, L3, R2, L2, R2, L1, R5, R3, R4, L5, R2, R5, L5, R1, R2, L1, L3, R3, R1, R3, L4, R4, L4, L1, R1, L2, L2, L4, R1, L3, R4, L2, R3, L1, L5, R4, R5, R2, R5, R1, R5, R1, R3, L3, L2, L2, L5, R2, L2, R5, R5, L2, R3, L5, R5, L2, R4, R2, L1, R3, L5, R3, R2, R5, L1, R3, L2, R2, R1";

enum Direction
{
	North = 0,
	East = 1,
	South = 2,
	West = 3
}

void Main()
{
	var commands = input.Split(new[] { ", " }, StringSplitOptions.None);
	var x = 0;
	var y = 0;
	var direction = Direction.North;
    foreach (var command in commands)
	{
		direction = (Direction)(((int)direction + (command[0] == 'R' ? 1 : -1) + 4) % 4);
		var distance = Int32.Parse(command.Substring(1));
		switch (direction)
		{
			case Direction.North:
				y += distance;
				break;
			case Direction.East:
				x += distance;
				break;
			case Direction.South:
				y -= distance;
				break;
			case Direction.West:
				x -= distance;
				break;
		}
	}
	var result = Math.Abs(x) + Math.Abs(y);
	result.Dump();
}
