<Query Kind="Program" />

static int input = 1358;
static Coords initialCoords = new Coords(1, 1);
static Coords targetCoords = new Coords(31, 39);

struct Coords
{
	public int X { get; private set; }
	public int Y { get; private set; }

	public Coords(int x, int y)
	{
		X = x;
		Y = y;
	}
}

struct State
{
	public Coords Coords { get; private set; }
	public int Steps { get; private set; }
	
	public State(Coords coords, int steps)
	{
		Coords = coords;
		Steps = steps;
	}
}

bool IsWall(Coords coords)
{
	var sum = coords.X * coords.X + 3 * coords.X + 2 * coords.X * coords.Y + coords.Y + coords.Y * coords.Y + input;
	var binaryString = Convert.ToString(sum, 2);
	var bitCount = binaryString.ToCharArray().Count(c => c == '1');
	return bitCount % 2 == 1;
}

IEnumerable<Coords> GetCandidates(Coords current)
{
	yield return new Coords(current.X + 1, current.Y);
	if (current.X > 0)
	{
		yield return new Coords(current.X - 1, current.Y);
	}
	yield return new Coords(current.X, current.Y + 1);
	if (current.Y > 0)
	{
		yield return new Coords(current.X, current.Y - 1);
	}
}

void Main()
{
	var visited = new HashSet<Coords>();
	var nextStates = new Queue<State>();
	nextStates.Enqueue(new State(new Coords(1, 1), 0));
	visited.Add(initialCoords);
	while (nextStates.Count > 0)
	{
		var currentState = nextStates.Dequeue();
		foreach (var candidate in GetCandidates(currentState.Coords))
		{
			if (candidate.Equals(targetCoords))
			{
				(currentState.Steps + 1).Dump();
				return;
			}
			if (visited.Contains(candidate))
            {
				continue;
			}
			else
			{
				visited.Add(candidate);
			}
			if (!IsWall(candidate))
			{
				nextStates.Enqueue(new State(candidate, currentState.Steps + 1));
			}
		}
	}
}

