<Query Kind="Program">
  <Namespace>System.Security.Cryptography</Namespace>
</Query>

static string input = "reyedfim";

void Main()
{
	var password = new StringBuilder();
	var index = 0;
	var md5 = MD5.Create();
	while (password.Length < 8)
	{
		var hash = BitConverter.ToString(md5.ComputeHash(Encoding.ASCII.GetBytes($"{input}{index}"))).Replace("-", String.Empty);
		if (hash.StartsWith("00000"))
        {
			password.Append(hash[5]);
		}
		index++;
	}
	password.ToString().ToLower().Dump();
}
