<Query Kind="Program" />

static int input = 3017957;

void Main()
{
	var nexts = new int[input];
	for (int i = 0; i < nexts.Length; i++)
	{
		nexts[i] = (i + 1) % input;
	}
	var current = 0;
	while (true)
	{
		nexts[current] = nexts[nexts[current]];
		if (nexts[current] == current)
		{
			(current + 1).Dump();
			return;
		}
		current = nexts[current];
	}
}
