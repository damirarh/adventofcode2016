<Query Kind="Program" />

static string firstRow = ".^^^^^.^^^..^^^^^...^.^..^^^.^^....^.^...^^^...^^^^..^...^...^^.^.^.......^..^^...^.^.^^..^^^^^...^.";
static int rowCount = 400000;

string NextRow(string row)
{
	var expandedRow = $".{row}.";
	var nextRow = new StringBuilder();
	for (int i = 1; i <= row.Length; i++)
	{
		var left = expandedRow[i - 1];
		var center = expandedRow[i];
		var right = expandedRow[i + 1];
		var trap = false;
		if (left == '^' && center == '^' && right == '.')
		{
			trap = true;
		}
		else if (center == '^' && right == '^' && left == '.')
		{
			trap = true;
		}
		else if (left == '^' && center == '.' && right == '.')
		{
			trap = true;
		}
		else if (right == '^' && center == '.' && left == '.')
		{
			trap = true;
		}
		nextRow.Append(trap ? '^' : '.');
	}
	return nextRow.ToString();
}

int CountSafeTiles(string row)
{
	return row.ToCharArray().Count(c => c == '.');
}

void Main()
{
	var safeTiles = CountSafeTiles(firstRow);
	var currentRow = firstRow;
	for (int i = 1; i < rowCount; i++)
	{
		currentRow = NextRow(currentRow);
		safeTiles += CountSafeTiles(currentRow);
	}
	safeTiles.Dump();
}
