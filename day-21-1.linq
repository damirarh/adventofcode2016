<Query Kind="Program" />

static string startPassword = "abcdefgh";
static string input = @"rotate left 2 steps
rotate right 0 steps
rotate based on position of letter a
rotate based on position of letter f
swap letter g with letter b
rotate left 4 steps
swap letter e with letter f
reverse positions 1 through 6
swap letter b with letter d
swap letter b with letter c
move position 7 to position 5
rotate based on position of letter h
swap position 6 with position 5
reverse positions 2 through 7
move position 5 to position 0
rotate based on position of letter e
rotate based on position of letter c
rotate right 4 steps
reverse positions 3 through 7
rotate left 4 steps
rotate based on position of letter f
rotate left 3 steps
swap letter d with letter a
swap position 0 with position 1
rotate based on position of letter a
move position 3 to position 6
swap letter e with letter g
move position 6 to position 2
reverse positions 1 through 2
rotate right 1 step
reverse positions 0 through 6
swap letter e with letter h
swap letter f with letter a
rotate based on position of letter a
swap position 7 with position 4
reverse positions 2 through 5
swap position 1 with position 2
rotate right 0 steps
reverse positions 5 through 7
rotate based on position of letter a
swap letter f with letter h
swap letter a with letter f
rotate right 4 steps
move position 7 to position 5
rotate based on position of letter a
reverse positions 0 through 6
swap letter g with letter c
reverse positions 5 through 6
reverse positions 3 through 5
reverse positions 4 through 6
swap position 3 with position 4
move position 4 to position 2
reverse positions 3 through 4
rotate left 0 steps
reverse positions 3 through 6
swap position 6 with position 7
reverse positions 2 through 5
swap position 2 with position 0
reverse positions 0 through 3
reverse positions 3 through 5
rotate based on position of letter d
move position 1 to position 2
rotate based on position of letter c
swap letter e with letter a
move position 4 to position 1
reverse positions 5 through 7
rotate left 1 step
rotate based on position of letter h
reverse positions 1 through 7
rotate based on position of letter f
move position 1 to position 5
reverse positions 1 through 4
rotate based on position of letter a
swap letter b with letter c
rotate based on position of letter g
swap letter a with letter g
swap position 1 with position 0
rotate right 2 steps
rotate based on position of letter f
swap position 5 with position 4
move position 1 to position 0
swap letter f with letter b
swap letter f with letter h
move position 1 to position 7
swap letter c with letter b
reverse positions 5 through 7
rotate left 6 steps
swap letter d with letter b
rotate left 3 steps
swap position 1 with position 4
rotate based on position of letter a
rotate based on position of letter a
swap letter b with letter c
swap letter e with letter f
reverse positions 4 through 7
rotate right 0 steps
reverse positions 2 through 3
rotate based on position of letter a
reverse positions 1 through 4
rotate right 1 step";

void Main()
{
	var swapPosRegex = new Regex("swap position (?<X>.) with position (?<Y>.)");
	var swapLetterRegex = new Regex("swap letter (?<X>.) with letter (?<Y>.)");
	var rotateLeftRegex = new Regex("rotate left (?<X>.) step");
	var rotateRightRegex = new Regex("rotate right (?<X>.) step");
	var rotateBasedRegex = new Regex("rotate based on position of letter (?<X>.)");
	var reverseRegex = new Regex("reverse positions (?<X>.) through (?<Y>.)");
	var movePosRegex = new Regex("move position (?<X>.) to position (?<Y>.)");

	var password = startPassword.ToCharArray();
	var operations = input.Split(new[] { Environment.NewLine }, StringSplitOptions.None);
	foreach (var operation in operations)
	{
		var match = swapPosRegex.Match(operation);
		if (match.Success)
		{
			var x = Int32.Parse(match.Groups["X"].Value);
			var y = Int32.Parse(match.Groups["Y"].Value);
			var c = password[x];
			password[x] = password[y];
			password[y] = c;
			continue;
		}
		match = swapLetterRegex.Match(operation);
		if (match.Success)
		{
			var x = match.Groups["X"].Value[0];
			var y = match.Groups["Y"].Value[0];
			for (int i = 0; i < password.Length; i++)
			{
				if (password[i] == x)
				{
					password[i] = y;
				}
				else if (password[i] == y)
				{
					password[i] = x;
				}
			}
			continue;
		}
		match = rotateLeftRegex.Match(operation);
		if (match.Success)
		{
			var x = Int32.Parse(match.Groups["X"].Value);
			for (int iteration = 0; iteration < x; iteration++)
			{
				var first = password[0];
				for (int i = 0; i < password.Length - 1; i++)
				{
					password[i] = password[i + 1];
				}
				password[password.Length - 1] = first;
			}
			continue;
		}
		match = rotateRightRegex.Match(operation);
		if (match.Success)
		{
			var x = Int32.Parse(match.Groups["X"].Value);
			for (int iteration = 0; iteration < x; iteration++)
			{
				var last = password[password.Length - 1];
				for (int i = password.Length - 1; i > 0; i--)
				{
					password[i] = password[i - 1];
				}
				password[0] = last;
			}
			continue;
		}
		match = rotateBasedRegex.Match(operation);
		if (match.Success)
		{
			var x = match.Groups["X"].Value[0];
			var index = new String(password).IndexOf(x);
			var repeat = index + 1 + (index >= 4 ? 1 : 0);
			for (int iteration = 0; iteration < repeat; iteration++)
			{
				var last = password[password.Length - 1];
				for (int i = password.Length - 1; i > 0; i--)
				{
					password[i] = password[i - 1];
				}
				password[0] = last;
			}
			continue;
		}
		match = reverseRegex.Match(operation);
		if (match.Success)
		{
			var x = Int32.Parse(match.Groups["X"].Value);
			var y = Int32.Parse(match.Groups["Y"].Value);
			for (int i = 0; i < (y - x + 1) / 2; i++)
			{
				var c = password[x + i];
				password[x + i] = password[y - i];
				password[y - i] = c;
			}
			continue;
		}
		match = movePosRegex.Match(operation);
		if (match.Success)
		{
			var x = Int32.Parse(match.Groups["X"].Value);
			var y = Int32.Parse(match.Groups["Y"].Value);
			var c = password[x];
			if (x < y)
			{
				for (int i = x; i < y; i++)
				{
					password[i] = password[i + 1];
				}
			}
			else
			{
				for (int i = x; i > y; i--)
				{
					password[i] = password[i - 1];
				}
			}
			password[y] = c;
		}
	}
	new String(password).Dump();
}
