<Query Kind="Program">
  <Namespace>System.Security.Cryptography</Namespace>
</Query>

static string input = "reyedfim";

void Main()
{
	var password = new char[8];
	var index = 0;
	var charactersFound = 0;
	var md5 = MD5.Create();
	while (charactersFound < 8)
	{
		var hash = BitConverter.ToString(md5.ComputeHash(Encoding.ASCII.GetBytes($"{input}{index}"))).Replace("-", String.Empty);
		if (hash.StartsWith("00000"))
		{
			var passwordIndex = hash[5] - '0';
			if (passwordIndex >= 0 && passwordIndex <= 7)
			{
				if (password[passwordIndex] == '\0')
				{
					password[passwordIndex] = hash[6];
					charactersFound++;
				}
			}
		}
		index++;
	}
	new String(password).ToLower().Dump();
}
