<Query Kind="Program" />

static int[] lengths = { 13, 5, 17, 3, 7, 19 };
static int[] offsets = { 11, 0, 11, 0, 2, 17 };

void Main()
{
	int time = 0;
	while (true)
	{
		var passed = true;
		var disc = 0;
		while (passed && disc < lengths.Length)
		{
			passed = (offsets[disc] + time + disc + 1) % (lengths[disc]) == 0;
			disc++;
		}
		if (passed)
		{
			break;
		}
		time++;
	}
	time.Dump();
}
