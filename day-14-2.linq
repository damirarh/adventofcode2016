<Query Kind="Program">
  <Namespace>System.Security.Cryptography</Namespace>
</Query>

static string salt = "ahsbgdzn";
static Dictionary<int, string> hashes =new Dictionary<int, string>();

string CalculateHash(int index)
{
	string hash;
	if (hashes.TryGetValue(index, out hash))
	{
		return hash;
	}
	var md5 = MD5.Create();
	hash = $"{salt}{index}";
	for (int i = 0; i <= 2016; i++)
	{
		var input = Encoding.ASCII.GetBytes(hash);
		var hashBytes = md5.ComputeHash(input);
		hash = BitConverter.ToString(hashBytes).Replace("-", "").ToLower();
	}
	hashes[index] = hash;
	return hash;
}

bool IsKey(int keyIndex, char character)
{
	var pattern = new String(character, 5);
	for (int i = 1; i <= 1000; i++)
	{
		var hash = CalculateHash(keyIndex + i);
		if (hash.Contains(pattern))
		{
			return true;
		}
	}
	return false;
}

void Main()
{
	int index = 0;
	int keysFound = 0;
	var regex = new Regex(@"(.)\1\1");
	while (keysFound < 64)
	{
		var hash = CalculateHash(index);
		var match = regex.Match(hash);
		if (match.Success)
		{
			if (IsKey(index, match.Value[0]))
			{
				keysFound++;
				$"{keysFound}: {index}".Dump();
			}
		}
		index++;
	}
}
