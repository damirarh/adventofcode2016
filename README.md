# Advent of Code 2016

C# solutions for [Advent of Code 2016](http://adventofcode.com/2016), written as programs in [LINQPad](http://www.linqpad.net/).