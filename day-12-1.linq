<Query Kind="Program" />

static string input = @"cpy 1 a
cpy 1 b
cpy 26 d
jnz c 2
jnz 1 5
cpy 7 c
inc d
dec c
jnz c -2
cpy a c
inc a
dec b
jnz b -2
cpy c b
dec d
jnz d -6
cpy 19 c
cpy 11 d
inc a
dec d
jnz d -2
dec c
jnz c -5";

void Main()
{
	var registers = new Dictionary<string, int>
	{
		{ "a", 0 },
		{ "b", 0 },
		{ "c", 0 },
		{ "d", 0 },
	};
	var instructions = input.Split(new[] { Environment.NewLine }, StringSplitOptions.None);
	int instructionPointer = 0;
	while (instructionPointer < instructions.Length)
	{
		var instruction = instructions[instructionPointer];
		var tokens = instruction.Split(new[] { ' ' });
		int x = 0;
		switch (tokens[0])
		{
			case "cpy":
				if (!Int32.TryParse(tokens[1], out x))
				{
					x = registers[tokens[1]];
				}
				registers[tokens[2]] = x;
				instructionPointer++;
				break;
			case "inc":
				registers[tokens[1]] = registers[tokens[1]] + 1;
				instructionPointer++;
				break;
			case "dec":
				registers[tokens[1]] = registers[tokens[1]] - 1;
				instructionPointer++;
				break;
			case "jnz":
				if (!Int32.TryParse(tokens[1], out x))
				{
					x = registers[tokens[1]];
				}
				if (x == 0)
				{
					instructionPointer++;
				}
				else
				{
					instructionPointer += Int32.Parse(tokens[2]);
				}
				break;
		}
	}
	registers["a"].Dump();
}

