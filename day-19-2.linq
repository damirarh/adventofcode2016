<Query Kind="Program" />

static int input = 3017957;

void Main()
{
	var elfs = new List<int>();
	for (int i = 1; i <= input; i++)
	{
		elfs.Add(i);
	}
	var current = 0;
	while (elfs.Count > 1)
	{
		var exclude = (current + elfs.Count / 2) % elfs.Count;
		elfs.RemoveAt(exclude);
		if (exclude < current)
		{
			current--;
		}
		current = (current + 1) % elfs.Count;
	}
	elfs[0].Dump();
}