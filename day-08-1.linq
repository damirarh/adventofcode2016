<Query Kind="Program" />

static string input = @"rect 1x1
rotate row y=0 by 5
rect 1x1
rotate row y=0 by 6
rect 1x1
rotate row y=0 by 5
rect 1x1
rotate row y=0 by 2
rect 1x1
rotate row y=0 by 5
rect 2x1
rotate row y=0 by 2
rect 1x1
rotate row y=0 by 4
rect 1x1
rotate row y=0 by 3
rect 2x1
rotate row y=0 by 7
rect 3x1
rotate row y=0 by 3
rect 1x1
rotate row y=0 by 3
rect 1x2
rotate row y=1 by 13
rotate column x=0 by 1
rect 2x1
rotate row y=0 by 5
rotate column x=0 by 1
rect 3x1
rotate row y=0 by 18
rotate column x=13 by 1
rotate column x=7 by 2
rotate column x=2 by 3
rotate column x=0 by 1
rect 17x1
rotate row y=3 by 13
rotate row y=1 by 37
rotate row y=0 by 11
rotate column x=7 by 1
rotate column x=6 by 1
rotate column x=4 by 1
rotate column x=0 by 1
rect 10x1
rotate row y=2 by 37
rotate column x=19 by 2
rotate column x=9 by 2
rotate row y=3 by 5
rotate row y=2 by 1
rotate row y=1 by 4
rotate row y=0 by 4
rect 1x4
rotate column x=25 by 3
rotate row y=3 by 5
rotate row y=2 by 2
rotate row y=1 by 1
rotate row y=0 by 1
rect 1x5
rotate row y=2 by 10
rotate column x=39 by 1
rotate column x=35 by 1
rotate column x=29 by 1
rotate column x=19 by 1
rotate column x=7 by 2
rotate row y=4 by 22
rotate row y=3 by 5
rotate row y=1 by 21
rotate row y=0 by 10
rotate column x=2 by 2
rotate column x=0 by 2
rect 4x2
rotate column x=46 by 2
rotate column x=44 by 2
rotate column x=42 by 1
rotate column x=41 by 1
rotate column x=40 by 2
rotate column x=38 by 2
rotate column x=37 by 3
rotate column x=35 by 1
rotate column x=33 by 2
rotate column x=32 by 1
rotate column x=31 by 2
rotate column x=30 by 1
rotate column x=28 by 1
rotate column x=27 by 3
rotate column x=26 by 1
rotate column x=23 by 2
rotate column x=22 by 1
rotate column x=21 by 1
rotate column x=20 by 1
rotate column x=19 by 1
rotate column x=18 by 2
rotate column x=16 by 2
rotate column x=15 by 1
rotate column x=13 by 1
rotate column x=12 by 1
rotate column x=11 by 1
rotate column x=10 by 1
rotate column x=7 by 1
rotate column x=6 by 1
rotate column x=5 by 1
rotate column x=3 by 2
rotate column x=2 by 1
rotate column x=1 by 1
rotate column x=0 by 1
rect 49x1
rotate row y=2 by 34
rotate column x=44 by 1
rotate column x=40 by 2
rotate column x=39 by 1
rotate column x=35 by 4
rotate column x=34 by 1
rotate column x=30 by 4
rotate column x=29 by 1
rotate column x=24 by 1
rotate column x=15 by 4
rotate column x=14 by 1
rotate column x=13 by 3
rotate column x=10 by 4
rotate column x=9 by 1
rotate column x=5 by 4
rotate column x=4 by 3
rotate row y=5 by 20
rotate row y=4 by 20
rotate row y=3 by 48
rotate row y=2 by 20
rotate row y=1 by 41
rotate column x=47 by 5
rotate column x=46 by 5
rotate column x=45 by 4
rotate column x=43 by 5
rotate column x=41 by 5
rotate column x=33 by 1
rotate column x=32 by 3
rotate column x=23 by 5
rotate column x=22 by 1
rotate column x=21 by 2
rotate column x=18 by 2
rotate column x=17 by 3
rotate column x=16 by 2
rotate column x=13 by 5
rotate column x=12 by 5
rotate column x=11 by 5
rotate column x=3 by 5
rotate column x=2 by 5
rotate column x=1 by 5";

void Main()
{
	var operations = input.Split(new[] { Environment.NewLine }, StringSplitOptions.None);
	var rectRegex = new Regex(@"rect (?<A>\d+)x(?<B>\d+)");
	var rowRegex = new Regex(@"rotate row y=(?<A>\d+) by (?<B>\d+)");
	var columnRegex = new Regex(@"rotate column x=(?<A>\d+) by (?<B>\d+)");
	var screen = new[]
	{
		new BitArray(50),
		new BitArray(50),
		new BitArray(50),
		new BitArray(50),
		new BitArray(50),
		new BitArray(50)
	};
	foreach (var operation in operations)
	{
		if (operation.StartsWith("rect"))
		{
			var match = rectRegex.Match(operation);
			var a = Int32.Parse(match.Groups["A"].Value);
			var b = Int32.Parse(match.Groups["B"].Value);
			for (int y = 0; y < b; y++)
			{
				for (int x = 0; x < a; x++)
				{
					screen[y].Set(x, true);
				}
			}
		}
		else if (operation.StartsWith("rotate row"))
		{
			var match = rowRegex.Match(operation);
			var a = Int32.Parse(match.Groups["A"].Value);
			var b = Int32.Parse(match.Groups["B"].Value);
			for (int i = 0; i < b; i++)
			{
				var last = screen[a].Get(49);
				for (int offset = 49; offset > 0; offset--)
				{
					screen[a].Set(offset, screen[a].Get(offset - 1));
				}
				screen[a].Set(0, last);
            }
		}
		else
		{
			var match = columnRegex.Match(operation);
			var a = Int32.Parse(match.Groups["A"].Value);
			var b = Int32.Parse(match.Groups["B"].Value);
			for (int i = 0; i < b; i++)
			{
				var last = screen[5].Get(a);
				for (int offset = 5; offset > 0; offset--)
				{
					screen[offset].Set(a, screen[offset - 1].Get(a));
				}
				screen[0].Set(a, last);
			}
		}
	}
	screen.Sum(array => Enumerable.Range(0, 50).Sum(i => array.Get(i) ? 1 : 0)).Dump();
}
