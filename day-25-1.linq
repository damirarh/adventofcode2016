<Query Kind="Program" />

static string input = @"cpy a d
cpy 4 c
cpy 643 b
inc d
dec b
jnz b -2
dec c
jnz c -5
cpy d a
jnz 0 0
cpy a b
cpy 0 a
cpy 2 c
jnz b 2
jnz 1 6
dec b
dec c
jnz c -4
inc a
jnz 1 -7
cpy 2 b
jnz c 2
jnz 1 4
dec b
dec c
jnz 1 -4
jnz 0 0
out b
jnz a -19
jnz 1 -21";

struct State
{
	public int A { get; private set; }
	public int B { get; private set; }
	public int C { get; private set; }
	public int D { get; private set; }

	public State(Dictionary<string, int> registers)
	{
		A = registers["a"];
		B = registers["b"];
		C = registers["c"];
		D = registers["d"];
	}
}

void Main()
{
	var initialValue = 1;
	while (true)
	{
		var registers = new Dictionary<string, int>
		{
			{ "a", initialValue },
			{ "b", 0 },
			{ "c", 0 },
			{ "d", 0 },
		};
		var trace = new HashSet<State>();
		var instructions = input.Split(new[] { Environment.NewLine }, StringSplitOptions.None);
		int instructionPointer = 0;
		var nextClockValue = 0;
		bool validClock = true;
		while (validClock && instructionPointer < instructions.Length)
		{
			var instruction = instructions[instructionPointer];
			var tokens = instruction.Split(new[] { ' ' });
			int x = 0;
			int y = 0;
			switch (tokens[0])
			{
				case "cpy":
					if (!Int32.TryParse(tokens[1], out x))
					{
						x = registers[tokens[1]];
					}
					if (registers.Keys.Contains(tokens[2]))
					{
						registers[tokens[2]] = x;
					}
					instructionPointer++;
					break;
				case "inc":
					registers[tokens[1]] = registers[tokens[1]] + 1;
					instructionPointer++;
					break;
				case "dec":
					registers[tokens[1]] = registers[tokens[1]] - 1;
					instructionPointer++;
					break;
				case "jnz":
					if (!Int32.TryParse(tokens[1], out x))
					{
						x = registers[tokens[1]];
					}
					if (x == 0)
					{
						instructionPointer++;
					}
					else
					{
						if (!Int32.TryParse(tokens[2], out y))
						{
							y = registers[tokens[2]];
						}
						instructionPointer += y;
					}
					break;
				case "tgl":
					if (!Int32.TryParse(tokens[1], out x))
					{
						x = registers[tokens[1]];
					}
					var targetInstruction = instructionPointer + x;
					if (targetInstruction >= 0 && targetInstruction < instructions.Length)
					{
						instructions[targetInstruction] = ToggleInstruction(instructions[targetInstruction]);
					}
					instructionPointer++;
					break;
				case "out":
					if (!Int32.TryParse(tokens[1], out x))
					{
						x = registers[tokens[1]];
					}
					if (x != nextClockValue)
					{
						validClock = false;
					}
					nextClockValue = 1 - nextClockValue;
					var state = new State(registers);
					if (trace.Contains(state))
					{
						initialValue.Dump();
						return;
					}
					trace.Add(state);
					instructionPointer++;
					break;
			}
		}
		initialValue++;
	}
}

string ToggleInstruction(string original)
{
	var tokens = original.Split(new[] { ' ' });
	switch (tokens[0])
	{
		case "inc":
			return original.Replace("inc", "dec");
		case "dec":
			return original.Replace("dec", "inc");
		case "tgl":
			return original.Replace("tgl", "inc");
		case "jnz":
			return original.Replace("jnz", "cpy");
		case "cpy":
			return original.Replace("cpy", "jnz");
	}
	return null;
}