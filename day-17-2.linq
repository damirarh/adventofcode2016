<Query Kind="Program">
  <Namespace>System.Security.Cryptography</Namespace>
</Query>

static string passcode = "gdjjyniy";

static char[] openValues = "bcdef".ToCharArray();

class State
{
	public string Path { get; private set; }
	public int X { get; private set; }
	public int Y { get; private set; }

	public State(int x, int y, string path)
	{
		X = x;
		Y = y;
		Path = path;
	}
}

string CalculateHash(string path)
{
	var input = Encoding.ASCII.GetBytes($"{passcode}{path}");
	var md5 = MD5.Create();
	return BitConverter.ToString(md5.ComputeHash(input)).Replace("-", "").ToLower();
}

IEnumerable<State> GetCandidates(State state)
{
	var hash = CalculateHash(state.Path);
	if (state.Y > 0 && openValues.Contains(hash[0]))
	{
		// move up
		yield return new State(state.X, state.Y - 1, state.Path + "U");
	}
	if (state.Y < 3 && openValues.Contains(hash[1]))
	{
		// move down
		yield return new State(state.X, state.Y + 1, state.Path + "D");
	}
	if (state.X > 0 && openValues.Contains(hash[2]))
	{
		// move up
		yield return new State(state.X - 1, state.Y, state.Path + "L");
	}
	if (state.X < 3 && openValues.Contains(hash[3]))
	{
		// move up
		yield return new State(state.X + 1, state.Y, state.Path + "R");
	}
}

bool IsDone(State state)
{
	return state.X == 3 && state.Y == 3;
}

void Main()
{
	var nextStates = new Queue<State>();
	nextStates.Enqueue(new State(0, 0, ""));
	State longestPath = null;
	while (nextStates.Count > 0)
	{
		var current = nextStates.Dequeue();
		foreach (var candidate in GetCandidates(current))
		{
			if (IsDone(candidate))
			{
				longestPath = candidate;
			}
			else
			{
				nextStates.Enqueue(candidate);
			}
		}
	}
	longestPath.Path.Length.Dump();
}
