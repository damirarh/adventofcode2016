<Query Kind="Program" />

static string initialState = "11110010111001001";
static int length = 35651584;

string NextDragon(string original)
{
	var result = new StringBuilder();
	result.Append(original);
	result.Append("0");
	for (int i = original.Length - 1; i >= 0; i--)
	{
		result.Append(original[i] == '1' ? '0' : '1');
	}
	return result.ToString();
}

string CalculateChecksum(string data)
{
	var checksum = new StringBuilder();
	for (int i = 0; i < data.Length; i = i + 2)
	{
		checksum.Append(data[i] == data[i + 1] ? '1' : '0');
	}
	return checksum.ToString();
}

void Main()
{
	var data = initialState;
	while (data.Length < length)
	{
		data = NextDragon(data);
	}
	data = data.Substring(0, length);
	var checksum = data;
	do
	{
		checksum = CalculateChecksum(checksum);
	} while (checksum.Length % 2 == 0);
	checksum.Dump();
}
